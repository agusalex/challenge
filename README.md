# Emi Labs Challenge

Welcome to our technical challenge!

We're excited to have you here and hope this challenge to be a great opportunity for you to demonstrate your capabilities.

Inside this repository, you'll find a folder for each exercise that we ask you to solve.  

We wish you the best of luck and happy coding! 🙌
## Doubts? Questions?

Feel free to reach out to:
- Pablo: pablo.giudice@emilabs.ai
- Andy:  andy@emilabs.ai
- Martin: martin@emilabs.ai
- Sofi: sofia.cortes@emilabs.ai
- Rodri: rodrigo.sevil@emilabs.ai
- Feche: feche@emilabs.ai
- Marin: marina.huberman@emilabs.ai
- Gabi: gabriel.dalborgo@emilabs.ai
